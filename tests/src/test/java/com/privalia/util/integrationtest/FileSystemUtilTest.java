package com.privalia.util.integrationtest;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

import org.junit.BeforeClass;
import org.junit.Test;

import com.privalia.util.FileSystemUtil;
import com.privalia.util.IFileSystemUtil;

public class FileSystemUtilTest {

    static IFileSystemUtil sut;

    @BeforeClass
    public static void setUp() {
	sut = FileSystemUtil.singleton();
    }

    @Test
    public void createFileForNewFiles() throws IOException {
	String newFilePath = "/tmp/newFile" + UUID.randomUUID() + ".test";
	// exercise
	File file = sut.createFile(newFilePath);
	assertEquals(file.getAbsolutePath(), newFilePath);
	assertTrue(file.exists());
	// cleanup
	file.delete();
    }

    @Test
    public void createFileForExistingFiles() throws IOException {
	String randomFilePath = "/tmp/newFile" + UUID.randomUUID() + ".test";
	File existingFile = createTestFile(randomFilePath);

	// exercise
	File file = sut.createFile(randomFilePath);
	BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
	String actualFileContent = bufferedReader.readLine();
	bufferedReader.close();

	assertEquals(randomFilePath, actualFileContent);

	// cleanup
	existingFile.delete();
    }

    private File createTestFile(String randomFilePath) throws IOException {
	File existingFile = new File(randomFilePath);
	existingFile.createNewFile();
	FileWriter writer = new FileWriter(existingFile);
	writer.write(randomFilePath);
	writer.close();
	return existingFile;
    }

    @Test(expected = IllegalArgumentException.class)
    public void getFileForNonExistantFilesThrowsException() {
	String randomFilePath = "/tmp/newFile" + UUID.randomUUID() + ".test";
	sut.getFile(randomFilePath);
    }

    @Test
    public void getFileForExistantFiles() throws IOException {
	String randomFilePath = "/tmp/newFile" + UUID.randomUUID() + ".test";
	File existingFile = createTestFile(randomFilePath);

	File actualFile = sut.getFile(randomFilePath);
	assertEquals(existingFile, actualFile);
    }

}
