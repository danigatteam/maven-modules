package com.privalia.dao;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import com.privalia.model.PrivaliaObject;
import com.privalia.util.IFileSystemUtil;
import com.privalia.util.IFileUtil;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Dao implements IDao<PrivaliaObject> {

    @NonNull
    Properties properties;

    @NonNull
    IFileSystemUtil fileSystemUtil;

    @NonNull
    IFileUtil fileUtil;

    @NonNull
    ISerializer serializer;

    @Override
    public boolean add(PrivaliaObject model) throws IOException {
	String filePath = properties.getProperty("filename");
	File file = fileSystemUtil.createFile(filePath);
	String studentString = serializer.serialize(model);
	return fileUtil.appendToFile(file, studentString);
    }

}