package com.privalia.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@RequiredArgsConstructor
public class Student extends PrivaliaObject {

    @NonNull
    private Integer idStudent;
    @NonNull
    private String name;
    @NonNull
    private String surname;
    @NonNull
    private Integer age;

    @Override
    public String toString() {
	StringBuilder builder = new StringBuilder();
	builder.append(idStudent);
	builder.append(",");
	builder.append(name);
	builder.append(",");
	builder.append(surname);
	builder.append(",");
	builder.append(age);
	builder.append(",");
	builder.append(super.getUuid());
	return builder.toString();
    }

}
