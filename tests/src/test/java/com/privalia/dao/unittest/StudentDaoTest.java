package com.privalia.dao.unittest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.privalia.dao.Dao;
import com.privalia.dao.ISerializer;
import com.privalia.dao.JsonSerializer;
import com.privalia.dao.StringSerializer;
import com.privalia.enums.Format;
import com.privalia.model.Student;
import com.privalia.util.IFileSystemUtil;
import com.privalia.util.IFileUtil;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

public class StudentDaoTest {

    @Mock
    Properties properties;
    @Mock
    IFileSystemUtil fileSystemUtil;
    @Mock
    IFileUtil fileUtil;
    @Mock
    ISerializer serializer;
    @Mock
    Student student;
    
    
    Dao sut;

    @Before
    public void setUp() {
	MockitoAnnotations.initMocks(this);
	
    }

    @Test
    public void testAddString() throws IOException {

	sut = new Dao(properties, fileSystemUtil, fileUtil, serializer);
	
	String filePath = "test";
	File file = mock(File.class);

	// mock file path in configuration
	when(properties.getProperty("filename")).thenReturn(filePath);
	// mock file
	when(fileSystemUtil.createFile(filePath)).thenReturn(file);
	// mock serializer
	String expectedFileContents = "123";
	when(serializer.serialize(student)).thenReturn(expectedFileContents);

	boolean expectedResult = true;
	when(fileUtil.appendToFile(file, expectedFileContents)).thenReturn(expectedResult);

	boolean actualResult = sut.add(student);

	assertEquals(expectedResult, actualResult);

	verify(properties, times(1)).getProperty("filename");
	verify(fileSystemUtil, times(1)).createFile(filePath);
	verify(fileUtil, times(1)).appendToFile(file, expectedFileContents);
	verify(serializer, times(1)).serialize(student);
    }

    @Test
    public void testAddJson() throws IOException {
	
	sut = new Dao(properties, fileSystemUtil, fileUtil, serializer);

	String filePath = "test";
	File file = mock(File.class);

	// mock file path in configuration
	when(properties.getProperty("filename")).thenReturn(filePath);
	// mock file
	when(fileSystemUtil.createFile(filePath)).thenReturn(file);
	// mock serializer
	String expectedFileContents = "{\"test\": \"JSON test\"}";
	when(serializer.serialize(student)).thenReturn(expectedFileContents);

	boolean expectedResult = true;
	when(fileUtil.appendToFile(file, expectedFileContents)).thenReturn(expectedResult);

	boolean actualResult = sut.add(student);

	assertEquals(expectedResult, actualResult);

	verify(properties, times(1)).getProperty("filename");
	verify(fileSystemUtil, times(1)).createFile(filePath);
	verify(fileUtil, times(1)).appendToFile(file, expectedFileContents);
	verify(serializer, times(1)).serialize(student);
    }

}
