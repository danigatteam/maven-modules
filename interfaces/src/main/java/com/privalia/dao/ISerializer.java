package com.privalia.dao;

public interface ISerializer {

    String serialize(Object serializable);
}
