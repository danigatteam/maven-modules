package com.privalia.util;

import java.io.File;
import java.io.IOException;

public class FileSystemUtil implements IFileSystemUtil {

    private static final FileSystemUtil instance;
    
    static {
	instance = new FileSystemUtil();
    }
    
    private FileSystemUtil() {}
    
    public static FileSystemUtil singleton() {
	return instance;
    }
    
    @Override
    public File createFile(String filePath) throws IOException {
	File file = new File(filePath);
	if (!file.exists()) {
	    file.createNewFile();
	}
	return file;
    }

    @Override
    public File getFile(String filePath) {
	// TODO Auto-generated method stub
	File file = new File(filePath);
	if (!file.exists()) {
	    throw new IllegalArgumentException();
	}
	return file;
    }

}
