package com.privalia.util.integrationtest;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

import org.junit.BeforeClass;
import org.junit.Test;

import com.privalia.util.FileUtil;
import com.privalia.util.IFileUtil;

public class FileUtilTest {

    static IFileUtil sut;

    @BeforeClass
    public static void setUp() {
	sut = FileUtil.singleton();
    }
    
    @Test
    public void appendToFile() throws IOException {
	File file = createTestFile();
	
	String expectedContents = "test text added";
	
	sut.appendToFile(file, expectedContents);
	
	BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
	String actualContents = bufferedReader.readLine();
	bufferedReader.close();
	
	assertEquals(expectedContents, actualContents);
	
    }

    private File createTestFile() throws IOException {
	String randomFilePath = "/tmp/test" + UUID.randomUUID() + ".test";
	File file = new File(randomFilePath);
	file.createNewFile();
	return file;
    }

    @Test
    public void findLineContainingFindsMatch() throws IOException {
	File file = createTestFile();
	
	FileWriter writer = new FileWriter(file);
	BufferedWriter bWriter = new BufferedWriter(writer);
	bWriter.write("line 1");
	bWriter.write(System.lineSeparator());
	bWriter.write("line 2 and more text");
	bWriter.write(System.lineSeparator());
	bWriter.write("line 2");
	bWriter.write(System.lineSeparator());
	bWriter.close();
	
	String actualResult = sut.findLineContaining(file, "line 2");
	
	assertEquals("line 2 and more text", actualResult);
    }

   
}
