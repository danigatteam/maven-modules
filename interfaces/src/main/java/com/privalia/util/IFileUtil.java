package com.privalia.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public interface IFileUtil {

    /**
     * Append text to a file
     * 
     * @param file
     *            File to be appended
     * @param text
     *            Text to append
     * @return
     * @throws IOException 
     */
    boolean appendToFile(File file, String text) throws IOException;

    /**
     * Find the first line in a file containing a given text
     * 
     * @param file
     * @param text
     * @return
     * @throws FileNotFoundException 
     */
    String findLineContaining(File file, String text) throws FileNotFoundException;
}
