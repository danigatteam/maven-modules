package com.privalia.dao;

import java.io.IOException;

public interface IDao<T> {
    boolean add(T model) throws IOException;
}
