package com.privalia.util;

import java.io.File;
import java.io.IOException;

public interface IFileSystemUtil {

    File createFile(String filePath) throws IOException;

    File getFile(String filePath);

}
