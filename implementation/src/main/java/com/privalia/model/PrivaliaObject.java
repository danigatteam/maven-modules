package com.privalia.model;

import java.util.UUID;

public class PrivaliaObject {

    private UUID uuid;

    public PrivaliaObject() {
	setUuid(UUID.randomUUID());
    }

    public PrivaliaObject(UUID uuid) {
	this.setUuid(uuid);
    }

    public UUID getUuid() {
	return uuid;
    }

    private void setUuid(UUID uuid) {
	this.uuid = uuid;
    }

}
