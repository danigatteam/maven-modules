package com.privalia.dao.integrationtest;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.privalia.dao.Dao;
import com.privalia.dao.ISerializer;
import com.privalia.dao.JsonSerializer;
import com.privalia.dao.StringSerializer;
import com.privalia.enums.Format;
import com.privalia.model.Student;
import com.privalia.util.FileSystemUtil;
import com.privalia.util.FileUtil;
import com.privalia.util.IFileSystemUtil;
import com.privalia.util.IFileUtil;

public class StudentDaoTest {

    // properties, fileSystemUtil, fileUtil
    @Mock
    Properties properties;

    IFileSystemUtil fileSystemUtil;

    IFileUtil fileUtil;
    
    
    
    private Dao sut;
    
    @Before
    public void setUp() throws Exception {
	
	MockitoAnnotations.initMocks(this);
	
	fileSystemUtil = FileSystemUtil.singleton();
	fileUtil = FileUtil.singleton();
	
    }

    @Test
    public void testAddAsText() throws IOException {
	
	String filePath = System.getProperty("java.io.tmpdir") + "/test_" + UUID.randomUUID();
	// mock properties
	when(properties.getProperty("filename")).thenReturn(filePath);
	
	sut = new Dao(properties, fileSystemUtil, fileUtil, new StringSerializer());
	
	// create an array of students
	Student[] studentArray = addStudents(10);
	
	// test all students are found
	File file = new File(filePath);
	for(Student student : studentArray) {
	    String line = FileUtil.singleton().findLineContaining(file, student.toString());
	    assertEquals(student.toString(), line);
	}
    }

    @Test
    public void testAddAsJson() throws IOException {
	
	String filePath = System.getProperty("java.io.tmpdir") + "/test_" + UUID.randomUUID();
	// mock properties
	when(properties.getProperty("filename")).thenReturn(filePath);
	
	JsonSerializer serializer = new JsonSerializer();
	sut = new Dao(properties, fileSystemUtil, fileUtil, serializer);
	
	// create an array of students
	Student[] studentArray = addStudents(10);
	
	// test all students are found
	File file = new File(filePath);
	for(Student student : studentArray) {
	    String expectedLine = serializer.serialize(student);
	    String actualLine = FileUtil.singleton().findLineContaining(file, expectedLine);
	    assertEquals(expectedLine, actualLine);
	}
    }

    private Student[] addStudents(int arraySize) throws IOException {
	Student[] studentArray = new Student[arraySize];
	for(int counter = 0; counter < arraySize; counter++) {
	    studentArray[counter] = new Student(counter, "name " + String.valueOf(counter), "surname " + String.valueOf(counter), counter);
	}
	
	// add the students
	for(Student student : studentArray) {
	    sut.add(student);
	}
	return studentArray;
    }

}
