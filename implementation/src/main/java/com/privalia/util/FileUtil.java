package com.privalia.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import lombok.Cleanup;

public class FileUtil implements IFileUtil {

private static final IFileUtil instance;
    
    static {
	instance = new FileUtil();
    }
    
    private FileUtil() {}
    
    public static IFileUtil singleton() {
	return instance;
    }
    
    @Override
    public boolean appendToFile(File file, String text) throws IOException {
	FileWriter writer = new FileWriter(file, true);
	BufferedWriter bWriter = new BufferedWriter(writer);
	bWriter.write(text);
	bWriter.write(System.lineSeparator());
	bWriter.close();
	return true;
    }

    @Override
    public String findLineContaining(File file, String text) throws FileNotFoundException {
	@Cleanup
	Scanner scanner = new Scanner(file);
	Boolean isFound = false;
	
	String line = "";
	while(scanner.hasNextLine() && isFound == false) {
	    line = scanner.nextLine();
	    if(line.contains(text)) {
		isFound = true;
	    }
	}
	return (isFound == true) ? line : "";
    }

}
