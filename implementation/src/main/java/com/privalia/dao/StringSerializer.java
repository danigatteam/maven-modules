package com.privalia.dao;

public class StringSerializer implements ISerializer {

    @Override
    public String serialize(Object serializable) {
	return serializable.toString();
    }

}
