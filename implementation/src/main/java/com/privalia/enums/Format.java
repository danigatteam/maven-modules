package com.privalia.enums;

public enum Format {
    TEXT,
    JSON
}
