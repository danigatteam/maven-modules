package com.privalia.dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonSerializer implements ISerializer {

    @Override
    public String serialize(Object object) {
	String json = "";
	Gson gson = new GsonBuilder().create();
	json = gson.toJson(object);
	return json;
    }

}
